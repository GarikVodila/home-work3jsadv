// Даний об'єкт employee. Додайте до нього властивості age і salary, не змінюючи початковий об'єкт (має бути створено новий об'єкт, який включатиме всі необхідні властивості). Виведіть новий об'єкт у консоль.

const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}

const upEmployee = { ...employee, age: 52,salary: 'a lot'}
const newemployee = Object.assign({age: 52,salary: 'a lot'}, employee)

console.log(newemployee);